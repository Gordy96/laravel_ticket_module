<?php

namespace Johnny\TicketModule;

use Johnny\Model;
use Johnny\HidesAttributes;

class File extends Model
{
    use HidesAttributes;

    public $table = TM_FILE_TABLE;

    protected $fillable = [
        'file_type_id',
        'user_id',
        'file_path',
        'original_filename'
    ];

    protected $appends = [
        'filename'
    ];

    public function getFilenameAttribute(){
        return $this->original_filename;
    }

    public function type() {
        return $this->belongsTo(FileType::class, 'file_type_id', 'id', TM_FILE_TYPE_TABLE);
    }

    public function message() {
        return $this->belongsTo(Message::class);
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    private function endswith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    protected function check_file_exists() {
        $sep = DIRECTORY_SEPARATOR;
        $bpath = $this->type->name;

        if(!$this->endswith($bpath, $sep))
            $bpath .= $sep;

        if(file_exists($bpath . $this->file_path))
            return fopen($bpath . $this->filepath, 'rw');
        else
            return null;
    }

    public function file() {
        return $this->check_file_exists();
    }
}
