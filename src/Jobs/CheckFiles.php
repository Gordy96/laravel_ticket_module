<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Johnny\TicketModule\File;

class CheckFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const QUEUE = TM_PACKAGE_DB_PREFIX.'files.check';
    const DELAY = 1800;

    protected $ids;

    /**
     * Create a new job instance.
     * @param Collection $files
     */
    public function __construct($files)
    {
        $this->ids = $files->pluck('id');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach(File::with(['message','user','message.ticket'])->find($this->ids) as $file){
            if($file->message != null){
                $path = base_path("storage/app/public/tickets/{$file->message->ticket->id}/");
                $filename = "{$path}{$file->file_path}";
                if(file_exists($filename))
                    unlink($filename);
            }
        }
    }
}
