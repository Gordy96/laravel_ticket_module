<?php
define('TM_PACKAGE_DB_PREFIX', 'tm_');
define('TM_TICKET_CATEGORY_TABLE', TM_PACKAGE_DB_PREFIX . 'ticket_category');
define('TM_TICKET_THEME_TABLE', TM_PACKAGE_DB_PREFIX . 'ticket_theme');
define('TM_TICKET_STATUS_TABLE', TM_PACKAGE_DB_PREFIX . 'ticket_statuses');
define('TM_TICKET_PRIORITY_TABLE', TM_PACKAGE_DB_PREFIX . 'ticket_priorities');
define('TM_TICKET_TABLE', TM_PACKAGE_DB_PREFIX . 'tickets');
define('TM_MESSAGE_TABLE', TM_PACKAGE_DB_PREFIX . 'messages');
define('TM_FILE_TYPE_TABLE', TM_PACKAGE_DB_PREFIX . 'file_types');
define('TM_FILE_TABLE', TM_PACKAGE_DB_PREFIX . 'files');
define('TM_MESSAGE_FILE_TABLE', TM_PACKAGE_DB_PREFIX . 'message_file');