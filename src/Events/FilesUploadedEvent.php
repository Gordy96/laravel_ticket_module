<?php

namespace Johnny\TicketModule\Events;

use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Collection;
use Johnny\TicketModule\Message;

class FilesUploadedEvent
{
    use SerializesModels;

    public $files;
    /**
     * Create a new event instance.
     *
     * @param  Collection  $files
     */
    public function __construct(Collection $files)
    {
        $this->files = $files;
    }
}