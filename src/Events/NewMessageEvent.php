<?php

namespace Johnny\TicketModule\Events;

use Illuminate\Queue\SerializesModels;

use Johnny\TicketModule\Message;
use Johnny\TicketModule\Ticket;

class NewMessageEvent
{
    use SerializesModels;

    /**
     * @var Message
     */
    public $message;
    public $owner;
    /**
     * @var Ticket
     */
    public $ticket;

    /**
     * Create a new event instance.
     *
     * @param  Message  $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
        $this->owner = $message->user;
        $this->ticket = $message->ticket;
    }
}