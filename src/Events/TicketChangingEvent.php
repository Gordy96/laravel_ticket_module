<?php

namespace Johnny\TicketModule\Events;

use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Collection;
use Johnny\TicketModule\Message;
use Johnny\TicketModule\Ticket;

class TicketChangingEvent
{
    use SerializesModels;

    /**
     * @var Ticket
     */
    public $ticket;
    /**
     * Create a new event instance.
     *
     * @param  Ticket  $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }
}