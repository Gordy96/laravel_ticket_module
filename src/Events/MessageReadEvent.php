<?php

namespace Johnny\TicketModule\Events;

use Illuminate\Queue\SerializesModels;

use Johnny\TicketModule\Message;

class MessageReadEvent
{
    use SerializesModels;

    public $message;
    public $owner;
    public $ticket;
    public $other;

    /**
     * Create a new event instance.
     *
     * @param  Message  $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
        $this->ticket = $message->ticket;
        $this->owner = $message->user;
        $assigned = $message->ticket->assigned;
        $creator = $message->ticket->creator;
        $this->other = $this->owner->id === $assigned->id ? $assigned : $creator;
    }
}