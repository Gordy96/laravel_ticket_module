<?php

namespace Johnny\TicketModule;

use Johnny\StatusModel as Model;
use Johnny\HidesAttributes;

class TicketTheme extends Model
{
    use HidesAttributes;

    public $table = TM_TICKET_THEME_TABLE;

    protected $fillable = [
        'name', 'user_defined'
    ];

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

    public function category(){
        return $this->belongsTo(TicketCategory::class);
    }
}
