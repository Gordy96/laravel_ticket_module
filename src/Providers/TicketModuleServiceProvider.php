<?php

namespace Johnny\TicketModule\Providers;

use Illuminate\Support\ServiceProvider;
use Johnny\TicketModule\Events\MessageReadEvent;
use Johnny\TicketModule\Message;

class TicketModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        require_once __DIR__ . "/../local_config.php";

        Message::updated(function(Message $message){
            if($message->isDirty('read_at')){
                event(new MessageReadEvent($message));
            }
        });

        $this->publishes([
            __DIR__.'/../config/tickets.php' => config_path('tickets.php'),
        ]);
        $this->publishes([
            __DIR__ . '/../resources/assets/js' => base_path('/resources/assets/js'),
        ], 'resources');

        $this->publishes([
            __DIR__ . '/../resources/assets/css' => base_path('/resources/assets/css'),
        ], 'resources');

        $this->publishes([
            __DIR__ . '/../resources/assets/sass' => base_path('/resources/assets/sass'),
        ], 'resources');
        $this->loadMigrationsFrom(__DIR__ . '/../migrations/');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'tickets');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'tickets');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
