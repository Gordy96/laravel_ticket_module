<?php

namespace Johnny\TicketModule;

use Johnny\StatusModel as Model;
use Johnny\HidesAttributes;

class TicketStatus extends Model
{
    use HidesAttributes;

    public $table = TM_TICKET_STATUS_TABLE;

    protected $fillable = [
        'name'
    ];

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }
}
