<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateFileTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_FILE_TYPE_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('extension');
            $table->timestamps();
        });

        DB::table(TM_FILE_TYPE_TABLE)->insert([
            'name'=>'document',
            'extension'=>'.pdf,.docx?,.txt,.zip'
        ]);
        DB::table(TM_FILE_TYPE_TABLE)->insert([
            'name'=>'image',
            'extension'=>'.jpe?g,.png,.bmp,.gif'
        ]);
        DB::table(TM_FILE_TYPE_TABLE)->insert([
            'name'=>'video',
            'extension'=>'.mp4,.3gp'
        ]);
        DB::table(TM_FILE_TYPE_TABLE)->insert([
            'name'=>'audio',
            'extension'=>'.mp3,.vaw,.ogg'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_FILE_TYPE_TABLE);
    }
}
