<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_TICKET_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('assigned_user_id')->nullable()->unsigned()->default(null);
            $table->foreign('assigned_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('status_id')->unsigned()->default(1);
            $table->foreign('status_id')
                ->references('id')
                ->on(TM_TICKET_STATUS_TABLE)
                ->onDelete('restrict');

            $table->integer('priority_id')->unsigned()->default(1);
            $table->foreign('priority_id')
                ->references('id')
                ->on(TM_TICKET_PRIORITY_TABLE)
                ->onDelete('restrict');

            $table->integer('theme_id')->unsigned();
            $table->foreign('theme_id')
                ->references('id')
                ->on(TM_TICKET_THEME_TABLE);

            $table->timestamps();
        });

        DB::update("ALTER TABLE " . TM_TICKET_TABLE . " AUTO_INCREMENT = " . rand(1000000, 2000000) . ";");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_TICKET_TABLE);
    }
}
