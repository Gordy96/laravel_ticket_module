<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateTicketPrioritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_TICKET_PRIORITY_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
        });
        DB::table(TM_TICKET_PRIORITY_TABLE)->insert([
            'name'=>'low'
        ]);
        DB::table(TM_TICKET_PRIORITY_TABLE)->insert([
            'name'=>'normal'
        ]);
        DB::table(TM_TICKET_PRIORITY_TABLE)->insert([
            'name'=>'high'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_TICKET_PRIORITY_TABLE);
    }
}
