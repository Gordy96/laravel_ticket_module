<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_FILE_TABLE, function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('file_type_id')->unsigned();
            $table->foreign('file_type_id')
                ->references('id')
                ->on(TM_FILE_TYPE_TABLE)
                ->onDelete('cascade');

            $table->string('file_path');
            $table->string('original_filename');

            $table->timestamps();
        });
        DB::update("ALTER TABLE " . TM_FILE_TABLE . " AUTO_INCREMENT = " . rand(1000000, 2000000) . ";");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_FILE_TABLE);
    }
}
