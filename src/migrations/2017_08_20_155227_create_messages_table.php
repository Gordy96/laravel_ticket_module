<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_MESSAGE_TABLE, function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('ticket_id')->unsigned();
            $table->foreign('ticket_id')
                ->references('id')
                ->on(TM_TICKET_TABLE)
                ->onDelete('cascade');

            $table->text('text');

            $table->timestamp('read_at')->nullable();

            $table->timestamps();
        });
        DB::update("ALTER TABLE " . TM_MESSAGE_TABLE . " AUTO_INCREMENT = " . rand(1000000, 2000000) . ";");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_MESSAGE_TABLE);
    }
}
