<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateTicketStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_TICKET_STATUS_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
        });
        DB::table(TM_TICKET_STATUS_TABLE)->insert([
            'name'=>'open'
        ]);
        DB::table(TM_TICKET_STATUS_TABLE)->insert([
            'name'=>'answered'
        ]);
        DB::table(TM_TICKET_STATUS_TABLE)->insert([
            'name'=>'closed'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_TICKET_STATUS_TABLE);
    }
}
