<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessageFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_MESSAGE_FILE_TABLE, function(Blueprint $table){
            $table->increments('id');

            $table->integer('message_id')->unsigned();
            $table->foreign('message_id')
                ->references('id')
                ->on(TM_MESSAGE_TABLE)
                ->onDelete('cascade');

            $table->integer('file_id')->unsigned();
            $table->foreign('file_id')
                ->references('id')
                ->on(TM_FILE_TABLE)
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_MESSAGE_FILE_TABLE);
    }
}
