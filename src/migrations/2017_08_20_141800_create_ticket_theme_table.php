<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateTicketThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_TICKET_THEME_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on(TM_TICKET_CATEGORY_TABLE);
            $table->boolean('user_defined')->default(false);
        });
        DB::table(TM_TICKET_THEME_TABLE)->insert([
            'name'=>'Other',
            'category_id'=>1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_TICKET_THEME_TABLE);
    }
}
