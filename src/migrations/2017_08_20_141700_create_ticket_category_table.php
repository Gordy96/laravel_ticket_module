<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateTicketCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TM_TICKET_CATEGORY_TABLE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('user_defined')->default(false);
        });

        DB::table(TM_TICKET_CATEGORY_TABLE)->insert([
            'name'=>'Other'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TM_TICKET_CATEGORY_TABLE);
    }
}
