<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 19.01.18
 * Time: 16:00
 */

namespace Johnny\TicketModule;

trait InteractsWithTickets
{
    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function assigned_tickets(){
        return $this->hasMany(Ticket::class, 'assigned_user_id', 'id');
    }

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

    public function files(){
        return $this->hasMany(File::class);
    }
}