<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 06.04.18
 * Time: 18:01
 */

namespace Johnny\TicketModule\Http\Controllers;


interface FileStoreProvider
{
    /**
     * @param $file
     * @param string $ticket_id
     * @return string $filename
     */
    public function store($file, $ticket_id);

    /**
     * @param string $filename
     * @return bool
     */
    public function exists($filename);

    /**
     * @param string $filename
     * @return string
     */
    public function get($filename);

    /**
     * @param string $filename
     * @return bool
     */
    public function delete($filename);

}