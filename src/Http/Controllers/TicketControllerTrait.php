<?php

namespace Johnny\TicketModule\Http\Controllers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Collection;

// -Local models
use Illuminate\Validation\UnauthorizedException;
use Johnny\TicketModule\Exceptions\TicketClosedException;
use Johnny\TicketModule\Exceptions\UnsupportedFileTypeException;
use Johnny\TicketModule\FileType;
use Johnny\TicketModule\File;
use Johnny\TicketModule\Ticket;
use Johnny\TicketModule\Message;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Johnny\TicketModule\Events\FilesUploadedEvent;

trait TicketControllerTrait
{
    /**
     * @var FileStoreProvider
     */
    protected $store = null;
    protected function check_user_folder($user, $types){
        $root = base_path() . "/storage/app/public/" . $user->id;
        if(is_dir($root)){
            foreach($types as $type){
                $str = $root . DIRECTORY_SEPARATOR . $type->name;
                is_dir($str) ? : mkdir($str);
            }
        }
        else
            mkdir($root);
    }

    private function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    protected function save_file(Request $request, $id = null) {
        $user = Auth::user();

        if(!$user)
            throw new UnauthorizedHttpException('unauthorized', 'Only authorized users permitted to upload files');

        if(!$request->files->count())
            return false;
        if(!$id)
            $id = $request->get('tid');
        $request_files = $request->files->all();
        $types = FileType::all();
        $result = [];
        $ticket = Ticket::find($id);
        if($this->isAssoc($request_files) && count($request_files) == 1 && is_array($request_files[array_keys($request_files)[0]]))
            $request_files = $request_files[array_keys($request_files)[0]];

        $files = [];
        foreach ($request_files as $file){
            $filetype = null;
            foreach($types as $type){
                foreach(explode(',', $type->extension) as $ext)
                    if(preg_match("/".str_ireplace(".", "", $ext)."/", $file->getClientOriginalExtension()))
                        $filetype = $type;
            }
            if($filetype){
                array_push($files, [$file, $filetype]);
            } else {
                throw new UnsupportedFileTypeException("File type {$file->getClientOriginalExtension()} is not allowed for upload");
            }
        }
        foreach($files as $temp){
            $file = $temp[0];
            $filetype = $temp[1];

//            $path = 'public/tickets/'.$id;
//            $x = UploadedFile::createFromBase($file);
//            $fn = $x->store($path);
//            $fn = explode('/', $fn);
//            $fn = $fn[count($fn) - 1];
            $fn = $this->store->store($file, $id);
            $f = new File([
                'file_type_id'=>$filetype->id,
                'user_id'=>$user->id,
                'file_path'=>$fn,
                'original_filename'=>$file->getClientOriginalName()
            ]);
            $f->save();
            $f = $f->refresh();
            array_push($result, $f);
        }
        return collect($result);
    }

    protected function create_ticket(Request $request){
        $user = Auth::user();
        $ticket = new Ticket([
            'name'=>$request->get('name'),
            'user_id'=>$user->id,
            'theme_id'=>$request->get('theme')
        ]);

        if($request->has('priority'))
            $ticket->priority_id = $request->get('priority');
        $ticket->save();

        return $ticket;
    }

    protected function delete_ticket(Request $request, $id = null){
        if(!$id)
            $id = $request->get('tid');
        $user = Auth::user();
        $ticket = Ticket::find($id);
        $result = false;

        if(is_null($ticket))
            throw new NotFoundHttpException();
        if(!$this->canInteract($ticket, $user))
            throw new UnauthorizedException();

        $ticket->delete();
        $result = true;

        return $result;
    }

    public function load_messages(Request $request, $id = null){
        $portion = 20;
        $user = Auth::user();
        if(!$id)
            $id = $request->get('tid');
        $ticket = Ticket::find($id);

        if(is_null($ticket))
            throw new NotFoundHttpException();
        if(!$this->canInteract($ticket, $user))
            throw new UnauthorizedException();

        $messages = $ticket->messages()->with(['user', 'attachments'])->latest()->paginate($portion);
        $messages->makeHidden(['user_id', 'ticket_id']);
        return $messages;

    }

    public function send_message(Request $request, $id = null){
        if(!$id)
            $id = $request->get('tid');
        $text = $request->get('text');
        $ticket = Ticket::find($id);
        $user = Auth::user();
        $result = null;
        if(is_null($ticket))
            throw new NotFoundHttpException();
        if(!$this->canInteract($ticket, $user))
            throw new UnauthorizedException();
        if(!$ticket->isOpen())
            throw new TicketClosedException();

        $result = $ticket->messages()->save(new Message([
            'user_id'=>$user->id,
            'text'=>$text
        ]));
        $result->load('user');
        $result->makeHidden(['user_id', 'ticket_id']);

        return $result;
    }

    public function close_ticket(Request $request, $id = null) {
        if(!$id)
            $id = $request->get('tid');
        $ticket = Ticket::find($id);
        $user = Auth::user();
        $result = null;

        if(is_null($ticket))
            throw new NotFoundHttpException();
        if(!$this->canInteract($ticket, $user))
            throw new UnauthorizedException();
        if(!$ticket->isOpen())
            throw new TicketClosedException();

        $ticket->status_id = 3;
        $result = $ticket->save();

        return $result;
    }

    public function upload_file(Request $request, $id = null) {
        if(!$id)
            $id = $request->get('tid');
        $ticket = Ticket::find($id);
        $user = Auth::user();
        $result = null;

        if(is_null($ticket))
            throw new NotFoundHttpException();
        if(!$this->canInteract($ticket, $user))
            throw new UnauthorizedException();
        if(!$ticket->isOpen())
            throw new TicketClosedException();

        $result = $this->save_file($request, $id);
        event(new FilesUploadedEvent($result));

        return $result;
    }

    private function canInteract(Ticket $ticket, $user){
        return $ticket->user_id == $user->id || ($ticket->assigned_user_id != null && $ticket->assigned_user_id == $user->id);
    }
}
