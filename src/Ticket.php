<?php

namespace Johnny\TicketModule;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Johnny\Model;
use Johnny\HidesAttributes;
use Johnny\HasManyThroughManyRelation;

use \Illuminate\Database\Eloquent\Model as DModel;
use Johnny\TicketModule\Events\TicketChangedEvent;
use Johnny\TicketModule\Events\TicketChangingEvent;

class Ticket extends Model
{
    use HidesAttributes;
    use HasManyThroughManyRelation;

    public $table = TM_TICKET_TABLE;

    protected $fillable = [
        'name',
        'user_id',
        'assigned_user_id',
        'status_id',
        'priority_id',
        'theme_id'
    ];

    protected $dispatchesEvents = [
        'updated'=>TicketChangedEvent::class,
        'updating'=>TicketChangingEvent::class
    ];

    public function creator() {
        return $this->belongsTo('App\User', 'user_id', 'id', 'users');
    }

    public function assigned() {
        return $this->belongsTo('App\User', 'assigned_user_id', 'id', 'users');
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }

    public function attachments() {
        return $this->hasManyThroughMany(
            File::class,
            Message::class,
            'ticket_id',
            'id',
            'id',
            'file_id',
            'message_id',
            'tm_message_file'
        );
    }

    public function last_message(){
        return $this->hasOne(Message::class)->latest();
    }

    public function status() {
        return $this->belongsTo(TicketStatus::class);
    }

    public function priority() {
        return $this->belongsTo(TicketPriority::class);
    }

    public function theme(){
        return $this->belongsTo(TicketTheme::class);
    }

    public function category(){
        return $this->hasOneThrough(
            TicketCategory::class,
            TicketTheme::class,
            'category_id',
            'id',
            'theme_id',
            'id'
            );
    }

    public function isOpen(){
        return $this->status_id !== 3;
    }

    /**
     * Define a has-many-through relationship.
     *
     * @param  string  $related
     * @param  string  $through
     * @param  string|null  $firstKey
     * @param  string|null  $secondKey
     * @param  string|null  $localKey
     * @param  string|null  $secondLocalKey
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function hasOneThrough($related, $through, $firstKey = null, $secondKey = null, $localKey = null, $secondLocalKey = null)
    {
        $through = new $through;

        $firstKey = $firstKey ?: $this->getForeignKey();

        $secondKey = $secondKey ?: $through->getForeignKey();

        return $this->newHasOneThrough(
            $this->newRelatedInstance($related)->newQuery(), $this, $through,
            $firstKey, $secondKey, $localKey ?: $this->getKeyName(),
            $secondLocalKey ?: $through->getKeyName()
        );
    }
    /**
     * Instantiate a new HasManyThrough relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $farParent
     * @param  \Illuminate\Database\Eloquent\Model  $throughParent
     * @param  string  $firstKey
     * @param  string  $secondKey
     * @param  string  $localKey
     * @param  string  $secondLocalKey
     * @return \Johnny\TicketModule\HasOneThrough
     */
    protected function newHasOneThrough(Builder $query, DModel $farParent, DModel $throughParent, $firstKey, $secondKey, $localKey, $secondLocalKey)
    {
        return new HasOneThrough($query, $farParent, $throughParent, $firstKey, $secondKey, $localKey, $secondLocalKey);
    }
}
