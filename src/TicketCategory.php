<?php

namespace Johnny\TicketModule;

use Johnny\StatusModel as Model;
use Johnny\HidesAttributes;

class TicketCategory extends Model
{
    use HidesAttributes;

    public $table = TM_TICKET_CATEGORY_TABLE;

    protected $fillable = [
        'name', 'user_defined'
    ];

    public function tickets(){
        return $this->hasManyThrough(Ticket::class, TicketTheme::class);
    }

    public function themes(){
        return $this->hasMany(TicketTheme::class);
    }
}
