<?php

namespace Johnny\TicketModule;

use Johnny\Model;
use Johnny\HidesAttributes;
use Johnny\TicketModule\Events\NewMessageEvent;

class Message extends Model
{
    use HidesAttributes;

    public $table = TM_MESSAGE_TABLE;

    protected $fillable = [
        'ticket_id', 'user_id', 'text'
    ];

    protected $dispatchesEvents = [
        'created'=>NewMessageEvent::class
    ];

    public function ticket() {
        return $this->belongsTo(Ticket::class);
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function attachments() {
        return $this->belongsToMany(File::class, TM_MESSAGE_FILE_TABLE, 'message_id', 'file_id');
    }
}
