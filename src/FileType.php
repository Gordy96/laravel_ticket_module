<?php

namespace Johnny\TicketModule;

use Illuminate\Database\Eloquent\Model;
use Johnny\HidesAttributes;

class FileType extends Model
{
    use HidesAttributes;

    public $table = TM_FILE_TYPE_TABLE;

    protected $fillable = [
        'name', 'extension'
    ];

    public function attachments(){
        return $this->hasMany(File::class);
    }
}
